set -o vi

#aliases

alias ll="ls -la"
alias vi=vim

#add scripts to PATH

export PATH=$HOME/Repos/gitlab.com/toastycheesy5/dotfiles/scripts:$PATH
#export PS1='\[\e]0;\u@\h: .../\W\a\]${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]../\W\[\033[00m\]\$ '
export PS1='\[\033[1;31m\]\u@\h\[\033[1;33m\]:\[\033[1;36m\]../\W\[\033[1;33m\]\$\[\033[00m\] '
